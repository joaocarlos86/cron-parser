# Cron Parser

Simple cron expression parser that, given a valid cron expression, prints the expression in the following format:

```
java -jar target/cron-parser-1.0-SNAPSHOT-jar-with-dependencies.jar "*/25 0 1,15 * */5 /usr/bin/find"

minute          0 25 50
hour            0
day of month    1 15
month           1 2 3 4 5 6 7 8 9 10 11 12
day of week     1 6
command         /usr/bin/find
```

## Getting Started

### What is cron?
The software utility cron is a time-based job scheduler in Unix-like computer operating systems. 
Users that set up and maintain software environments use cron to schedule jobs (commands or shell scripts) to run 
periodically at fixed times, dates, or intervals. It typically automates system maintenance or 
administration — though its general-purpose nature makes it useful for things like downloading files from 
the Internet and downloading email at regular intervals.
[Source](https://en.wikipedia.org/wiki/Cron)

### What is a cron expression?
A cron expression is an expression that specifies the periodicity which a given command should be executed, a cron 
expression has the following format:

```
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (1 - 7) (Sunday to Saturday;
# │ │ │ │ │                                   
# │ │ │ │ │
# │ │ │ │ │
# * * * * * command to execute
```
[source]([https://en.wikipedia.org/wiki/Cron#Overview)

A cron expression can also have a few special characters, such as:
```
* (all) – it is used to specify that event should happen for every time unit. For example, “*” in the <minute> field – means “for every minute”
– (range) – it is used to determine the value range. For example, “10-11” in <hour> field means “10th and 11th hours”
? (any) – it is utilized in the <day-of-month> and <day-of -week> fields to denote the arbitrary value – neglect the field value.
, (values) – it is used to specify multiple values. For example, “1,2,3” in <day-of-week> field means on the days “Monday, Wednesday, and Friday”
/ (increments) – it is used to specify the incremental values. For example, a “5/15” in the <minute> field, means at “5, 20, 35 and 50 minutes of an hour”
```

Because of time constraints, this project handles only a subset of the characters specified above:

```aidl
"*" (all), "-" (range), "," (values) and "/" (increments)

The "/" (increments) operator may be used in combination with "*" (all) operator in the following format:
*/N -> where N is an arbitrary integer within the type limits
```

Examples of valid cron expressions handled by this parser are:

```
*/25 0 1,15 * */5 /usr/bin/find
* 1-2 */15 * * /usr/bin/find
25 10 1 1 1 /usr/bin/find
* * * * * /usr/bin/find
```

Examples of invalid cron expressions are:

```
-1/25 0 1,15 * */5 /usr/bin/find
* a */15 * * /usr/bin/find
25 1 /usr/bin/find
/usr/bin/find
```

### Prerequisites

To build and run this project you will need

```
Java 8
Apache Maven 3
```

### Installing

To install the application in your local maven repo, execute the following command in a terminal

```
mvn clean install
```

To build the application without installing in your local repository, run:

```
mvn clean package
```

To run the application, execute the following command in any terminal at the root directory of the application:

```
java -jar target/cron-parser-1.0-SNAPSHOT-jar-with-dependencies.jar "*/25 0 1,15 * */5 /usr/bin/find"
```

## Running the tests

To test the application, run the following command in the root directory of the application in a terminal window:

```
mvn clean test
```

You should expect to see the following report at the end of the execution:
```

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.deliveroo.parser.CronExpressionParserTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.077 sec
Running com.deliveroo.parser.SharedExpressionValidatorTest
Tests run: 12, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running com.deliveroo.parser.SharedExpressionParserTest
Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running com.deliveroo.parser.expressiontype.processor.MultipleValuesExpressionTypeToStringProcessorTest
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 sec
Running com.deliveroo.parser.expressiontype.processor.IncrementExpressionTypeToStringProcessorTest
Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 sec
Running com.deliveroo.parser.expressiontype.processor.DefaultExpressionTypeToStringProcessorTest
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running com.deliveroo.parser.expressiontype.processor.RangeOfValuesExpressionTypeToStringProcessorTest
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 sec
Running com.deliveroo.parser.expressiontype.processor.AllValuesExpressionTypeToStringProcessorTest
Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running com.deliveroo.parser.expressiontype.CronExpressionTypeTest
Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 sec
Running com.deliveroo.parser.expressiontype.CronExpressionToStringTest
Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 sec

Results :

Tests run: 57, Failures: 0, Errors: 0, Skipped: 0


```

## Running the Application

For convenience purposes, there is a copy of the generated binary file in the project's root directory. If a code change is 
introduced then the user should rebuild the application and use the new binary under the "target" directory.

### Built-in binary
```
 java -jar cron-parser-1.0-SNAPSHOT-jar-with-dependencies.jar "25 10 1 1 1 /usr/bin/find"
```

### Binary generated after code changes (after executing mvn package and/or mvn install)
```
 java -jar target/cron-parser-1.0-SNAPSHOT-jar-with-dependencies.jar "25 10 1 1 1 /usr/bin/find"
```

## Built With

* [JUnit](https://junit.org/junit4/) - JUnit is a simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
* [Maven](https://maven.apache.org/) - Dependency Management
* [Java](https://www.java.com) - Main runtime

## Authors

* **Joao Rodrigues** - *Initial work* - [joaocarlos86](https://github.com/joaocarlos86)