package com.deliveroo.parser;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.SharedExpressionValidator;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SharedExpressionValidatorTest {

    private SharedExpressionValidator validator = null;

    @Before
    public void setup(){
        validator = new SharedExpressionValidator();
    }

    @Test
    public void validate_givenExpressionUseIncrementsStartingAtAnyMinute_shouldReturnTrue() {
        final String expression = "*/15";

        Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsLetters_shouldThrowException() {
        final String expression = "a/15";
        Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
    }

    @Test
    public void validate_givenExpressionUseIncrementsStartingAtGivenMinute_shouldReturnTrue() {
        final String expression = "10/15";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionUseNegativeIncrements_shouldThrowException() {
        final String expression = "10/-15";
        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
    }

    @Test
    public void validate_givenExpressionUseSingleValue_shouldReturnTrue() {
        final String expression = "10";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionUseSingleValueAndValueContainsLetter_shouldThrowException() {
        final String expression = "10a";
        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
    }

    @Test
    public void validate_givenExpressionIsListOfValues_shouldReturnTrue() {
        final String expression = "10,20,30,40,50";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsListOfValuesAndNegativeValue_shouldThrowException() {
        final String expression = "10,20,30,40,-50";
        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsListOfValuesAndContainsLetter_shouldThrowException() {
        final String expression = "10,20,a,40,50";
        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
    }

    @Test
    public void validate_givenExpressionIsRangeOfValue_shouldReturnTrue() {
        final String expression = "10-20";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsRangeOfValueAndContainsNegativeNumber_shouldThrowException() {
        final String expression = "-10-20";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsRangeOfValueAndContainsLetter_shouldThrowException() {
        final String expression = "10-a";

        final Boolean validation = validator.validateExpression(expression, CronExpressionPart.MINUTE);
        assertEquals(true, validation);
    }

}
