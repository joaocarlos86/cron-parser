package com.deliveroo.parser;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.SharedExpressionParser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SharedExpressionParserTest {

    private SharedExpressionParser sharedExpressionParser = null;

    @Before
    public void setup(){
        sharedExpressionParser = new SharedExpressionParser();
    }

    @Test
    public void convertExpressionToString_givenExpressionUseIncrementsStartingAtAnyMinute_shouldReturnMinuteExpressionStartingAtAnyMinuteAndAllFollowingExecutions() {
        final String expression = "*/15";
        final String expectedPrintedExpression = "0 15 30 45";

        final String currentPrintedExpression = sharedExpressionParser.convertExpressionToString(expression, CronExpressionPart.MINUTE);
        assertEquals(expectedPrintedExpression, currentPrintedExpression);
    }

    @Test
    public void convertExpressionToString_givenExpressionUseIncrementsStartingAtGivenMinute_shouldReturnMinuteExpressionStartingAtGivenMinuteAndAllFollowingExecutions() {
        final String expression = "10/15";
        final String expectedPrintedExpression = "10 25 40 55";

        final String currentPrintedExpression = sharedExpressionParser.convertExpressionToString(expression, CronExpressionPart.MINUTE);
        assertEquals(expectedPrintedExpression, currentPrintedExpression);
    }

    @Test
    public void convertExpressionToString_givenExpressionUseSingleValue_shouldReturnMinuteExpressionWithSingleValue() {
        final String expression = "10";
        final String expectedPrintedExpression = "10";

        final String currentPrintedExpression = sharedExpressionParser.convertExpressionToString(expression, CronExpressionPart.MINUTE);
        assertEquals(expectedPrintedExpression, currentPrintedExpression);
    }

    @Test
    public void convertExpressionToString_givenExpressionIsListOfValues_shouldReturnMinuteExpressionWithAllExecutions() {
        final String expression = "10,20,30,40,50";
        final String expectedPrintedExpression = "10 20 30 40 50";

        final String currentPrintedExpression = sharedExpressionParser.convertExpressionToString(expression, CronExpressionPart.MINUTE);
        assertEquals(expectedPrintedExpression, currentPrintedExpression);
    }

    @Test
    public void convertExpressionToString_givenExpressionIsRangeOfValues_shouldReturnMinuteExpressionWithAllExecutionsWithinRange() {
        final String expression = "10-20";
        final String expectedPrintedExpression = "10 11 12 13 14 15 16 17 18 19 20";

        final String currentPrintedExpression = sharedExpressionParser.convertExpressionToString(expression, CronExpressionPart.MINUTE);
        assertEquals(expectedPrintedExpression, currentPrintedExpression);
    }
}
