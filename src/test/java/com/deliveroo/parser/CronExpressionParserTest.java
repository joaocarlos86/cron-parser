package com.deliveroo.parser;

import com.deliveroo.cron.CronExpressionParser;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CronExpressionParserTest {
    @Test
    public void toString_givenValidExpression_shouldPrintExpressionWithAllElements(){
        final String expression = "*/15 0 1,15 * 1-5 /usr/bin/find";

        final String expectedOutput =
                "minute\t\t0 15 30 45\n" +
                "hour\t\t0\n" +
                "day of month\t1 15\n" +
                "month\t\t1 2 3 4 5 6 7 8 9 10 11 12\n" +
                "day of week\t1 2 3 4 5\n" +
                "command\t\t/usr/bin/find";

        final CronExpressionParser parser = new CronExpressionParser(expression);

        assertEquals(expectedOutput, parser.toString());
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void toString_givenExpressionWithNegativeNumbersInMinute_shouldThrowException(){
        final String expression = "-1/15 0 1,15 * 1-5 /usr/bin/find";
        final CronExpressionParser parser = new CronExpressionParser(expression);
    }
}
