package com.deliveroo.parser.expressiontype;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.expressiontype.CronExpressionType;
import com.deliveroo.cron.expressiontype.CronExpressionToString;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CronExpressionToStringTest {

    private CronExpressionToString expressionToString;

    @Before
    public void setup(){
        this.expressionToString = new CronExpressionToString();
    }

    @Test
    public void expressionToString_givenExpressionContainsIncrement_shouldProcess(){
        final String expression = expressionToString
                .getStringValue("10/10", CronExpressionType.INCREMENT, CronExpressionPart.HOUR);

        assertNotNull(expression);
    }

    @Test
    public void expressionToString_givenExpressionContainsAllValue_shouldProcess(){
        final String expression = expressionToString
                .getStringValue("*", CronExpressionType.ALL_VALUES, CronExpressionPart.HOUR);
        assertNotNull(expression);
    }

    @Test
    public void expressionToString_givenExpressionContainsMultipleValues_shouldProcess(){
        final String expression = expressionToString
                .getStringValue("10,11,12", CronExpressionType.MULTIPLE_VALUES, CronExpressionPart.HOUR);
        assertNotNull(expression);
    }

    @Test
    public void expressionToString_givenExpressionContainsRangeOfValues_shouldProcess(){
        final String expression = expressionToString
                .getStringValue("10-20", CronExpressionType.RANGE_OF_VALUES, CronExpressionPart.HOUR);
        assertNotNull(expression);
    }

    @Test
    public void expressionToString_givenExpressionContainsNoSpecialCharacter_shouldProcess(){
        final String expression = expressionToString
                .getStringValue("10", CronExpressionType.SINGLE_VALUE, CronExpressionPart.HOUR);
        assertNotNull(expression);
    }

    @Test
    public void expressionToString_givenAnyExpressionNotSupported_shouldFallbackReturningTheOriginalExpression(){
        final String expression = expressionToString
                .getStringValue("this is an unsupported expression", CronExpressionType.SINGLE_VALUE, CronExpressionPart.HOUR);
        assertEquals("this is an unsupported expression",expression);
    }
}
