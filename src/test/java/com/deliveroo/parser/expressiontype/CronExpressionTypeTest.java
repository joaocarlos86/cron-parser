package com.deliveroo.parser.expressiontype;

import com.deliveroo.cron.expressiontype.CronExpressionType;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CronExpressionTypeTest {

    @Test
    public void getType_givenExpressionContainsIncrementIdentifier_shouldReturnIncrementType(){
        final String incrementExpression = "10/10";
        final CronExpressionType type = CronExpressionType.getType(incrementExpression);

        assertEquals(CronExpressionType.INCREMENT, type);
    }

    @Test
    public void getType_givenExpressionContainsMultipleValues_shouldReturnMultipleValuesType(){
        final String expression = "10,11,12";
        final CronExpressionType type = CronExpressionType.getType(expression);

        assertEquals(CronExpressionType.MULTIPLE_VALUES, type);
    }

    @Test
    public void getType_givenExpressionContainsRangeOfValues_shouldReturnRangeOfValuesType(){
        final String expression = "10-20";
        final CronExpressionType type = CronExpressionType.getType(expression);

        assertEquals(CronExpressionType.RANGE_OF_VALUES, type);
    }

    @Test
    public void getType_givenExpressionContainsSingleValue_shouldReturnSingleValueType(){
        final String expression = "10";
        final CronExpressionType type = CronExpressionType.getType(expression);

        assertEquals(CronExpressionType.SINGLE_VALUE, type);
    }

    @Test
    public void getType_givenExpressionContainsAllValues_shouldReturnAllValuesType(){
        final String expression = "*";
        final CronExpressionType type = CronExpressionType.getType(expression);

        assertEquals(CronExpressionType.ALL_VALUES, type);
    }

    @Test
    public void getType_givenExpressionContainsAllValuesWithIncrement_shouldReturnIncrementType(){
        final String expression = "*/10";
        final CronExpressionType type = CronExpressionType.getType(expression);

        assertEquals(CronExpressionType.INCREMENT, type);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void getType_givenExpressionContainsUnmappedOperator_shouldThrowException(){
        final String expression = "10+10";
        final CronExpressionType type = CronExpressionType.getType(expression);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void getType_givenExpressionContainsLetters_shouldThrowException(){
        final String expression = "10a";
        final CronExpressionType type = CronExpressionType.getType(expression);
    }
}
