package com.deliveroo.parser.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.MultipleValuesExpressionTypeToStringProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MultipleValuesExpressionTypeToStringProcessorTest {

    private MultipleValuesExpressionTypeToStringProcessor processor;

    @Before
    public void setup(){
        this.processor = new MultipleValuesExpressionTypeToStringProcessor();
    }

    @Test
    public void handleMultipleValues_givenExpressionIsAListOfValues_shouldReturnAllElementsInThatList() {
        final String expression = "1,4,6,8,10";
        final String parsedExpression = processor.handle(expression, CronExpressionPart.MINUTE);

        assertEquals("1 4 6 8 10", parsedExpression);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsValuesBelowLowerLimit_shouldThrowException(){
        processor.validate("-1,2,3", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsValuesAboveUpperLimit_shouldThrowException(){
        processor.validate("99,110", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsLetters_shouldThrowException(){
        processor.validate("a,b,c", CronExpressionPart.MINUTE);
    }
}
