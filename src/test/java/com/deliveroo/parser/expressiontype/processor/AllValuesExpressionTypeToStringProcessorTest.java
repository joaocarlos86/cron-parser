package com.deliveroo.parser.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.AllValuesExpressionTypeToStringProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AllValuesExpressionTypeToStringProcessorTest {

    private AllValuesExpressionTypeToStringProcessor processor;

    @Before
    public void setup(){
        processor = new AllValuesExpressionTypeToStringProcessor();
    }

    @Test
    public void handle_givenExpressionIsStar_shouldReturnAllValuesBetweenUpperAndLowerLimitForType(){
        final String expression = "*";
        final String parsedExpression = processor.handle(expression, CronExpressionPart.DAY_OF_WEEK);

        assertEquals("1 2 3 4 5 6 7", parsedExpression);
    }

    @Test
    public void validate_givenExpressionIsStar_shouldNotThrowException(){
        final String expression = "*";
        processor.validate(expression, CronExpressionPart.DAY_OF_WEEK);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsNotStar_shouldThrowException(){
        final String expression = "abc";
        processor.validate(expression, CronExpressionPart.DAY_OF_WEEK);
    }
}
