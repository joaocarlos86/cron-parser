package com.deliveroo.parser.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.IncrementExpressionTypeToStringProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IncrementExpressionTypeToStringProcessorTest {

    private IncrementExpressionTypeToStringProcessor processor;

    @Before
    public void setup(){
        this.processor = new IncrementExpressionTypeToStringProcessor();
    }

    @Test
    public void handle_givenExpressionContainsAnIncrementFromGivenMoment_shouldReturnAllValuesBeginningAtGivenMomentIncrementedByIncrementUpToUpperLimitForType(){
        final String expression = "10/15";
        final String parsedExpression = processor.handle(expression, CronExpressionPart.MINUTE);

        assertEquals("10 25 40 55", parsedExpression);
    }

    @Test
    public void handle_givenExpressionContainsAnIncrementFromStarOperator_shouldReturnAllValuesBeginningLowerLimitForTypeIncrementedByIncrementUpToUpperLimitForType(){
        final String expression = "*/15";
        final String parsedExpression = processor.handle(expression, CronExpressionPart.MINUTE);

        assertEquals("0 15 30 45", parsedExpression);

        final String dayOfWeekExpression = "*/5";
        final String dayOfWeekParsedExpression = processor.handle(dayOfWeekExpression, CronExpressionPart.DAY_OF_WEEK);

        assertEquals("1 6", dayOfWeekParsedExpression);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsNumberBelowLowerLimitInNumerator_shouldThrowException(){
        processor.validate("-1/10", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsLettersInTheInitialExecution_shouldThrowException(){
        processor.validate("a/10", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsLettersInTheIncrement_shouldThrowException(){
        processor.validate("10/a", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsStarCharacterInIncrementOperator_shouldThrowException(){
        processor.validate("10/*", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsEmpty_shouldThrowException(){
        processor.validate("", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsNegativeIncrement_shouldThrowException(){
        processor.validate("10/-10", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionContainsInitialExecutionOverUpperLimit_shouldThrowException(){
        processor.validate("99/10", CronExpressionPart.MINUTE);
    }

}
