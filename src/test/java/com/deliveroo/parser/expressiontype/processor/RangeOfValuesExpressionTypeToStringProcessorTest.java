package com.deliveroo.parser.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.RangeOfValuesExpressionTypeToStringProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RangeOfValuesExpressionTypeToStringProcessorTest {

    private RangeOfValuesExpressionTypeToStringProcessor processor;

    @Before
    public void setup(){
        this.processor = new RangeOfValuesExpressionTypeToStringProcessor();
    }

    @Test
    public void handle_givenExpressionIsARange_shouldReturnAllValuesWithinThatRange(){
        final String expression = "10-15";

        final String parsedExpression = processor.handle(expression, CronExpressionPart.MINUTE);
        assertEquals("10 11 12 13 14 15", parsedExpression);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validateRangeOfValues_givenExpressionContainsValuesBelowLowerLimit_shouldThrowException(){
        processor.validate("-1-3", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validateRangeOfValues_givenExpressionContainsValuesAboveUpperLimit_shouldThrowException(){
        processor.validate("99-110", CronExpressionPart.MINUTE);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validateRangeOfValues_givenExpressionContainsLetters_shouldThrowException(){
        processor.validate("a-c", CronExpressionPart.MINUTE);
    }
}
