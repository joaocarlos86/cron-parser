package com.deliveroo.parser.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.DefaultExpressionTypeToStringProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DefaultExpressionTypeToStringProcessorTest {

    private DefaultExpressionTypeToStringProcessor processor;

    @Before
    public void setup(){
        this.processor = new DefaultExpressionTypeToStringProcessor();
    }

    @Test
    public void handle_giveAnyExpression_shouldReturnSameExpression(){
        final String expression = "5";
        final String parsedExpression = processor.handle(expression, CronExpressionPart.DAY_OF_WEEK);

        assertEquals(expression, parsedExpression);
    }

    @Test
    public void validate_givenExpressionIsNumeric_shouldNotThrowException(){
        final String expression = "1";
        processor.validate(expression, CronExpressionPart.DAY_OF_WEEK);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsNotNumeric_shouldThrowException(){
        final String expression = "10a";
        processor.validate(expression, CronExpressionPart.DAY_OF_WEEK);
    }

    @Test(expected = InvalidCronElementExpression.class)
    public void validate_givenExpressionIsNotWithinTypeLimits_shouldThrowException(){
        final String expression = "10";
        processor.validate(expression, CronExpressionPart.DAY_OF_WEEK);
    }

}
