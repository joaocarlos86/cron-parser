package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.CronExpressionType;

public class DefaultExpressionTypeToStringProcessor implements CronExpressionTypeToStringProcessor {
    @Override
    public String handle(String expression, CronExpressionPart expressionPart) {
        return expression;
    }

    @Override
    public void validate(String expression, CronExpressionPart part) {
        if(isNumeric(expression) == false) {
            throw new InvalidCronElementExpression(part.toString(), expression, "Invalid element");
        }

        if(valueIsWithinAllowedInvervalForType(Integer.parseInt(expression), part) == false) {
            throw new InvalidCronElementExpression(part.toString(), expression, "Value should be within limits for type");
        }
    }

    @Override
    public Boolean supports(CronExpressionType expressionType) {
        return Boolean.TRUE;
    }
}
