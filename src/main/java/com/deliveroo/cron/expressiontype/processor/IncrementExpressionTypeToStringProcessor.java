package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.CronExpressionType;

public class IncrementExpressionTypeToStringProcessor implements CronExpressionTypeToStringProcessor {
    @Override
    public String handle(String expression, CronExpressionPart expressionPart) {
        final StringBuilder builder = new StringBuilder("");
        final String[] expressionParts = expression.split(CronExpressionType.INCREMENT.getIdentifier());
        final Integer increment = Integer.valueOf(expressionParts[1]);
        int executionInstant = expressionPart.getLowerLimit();
        if(expression.contains(CronExpressionType.ALL_VALUES.getIdentifier()) == false) {
            executionInstant = Integer.parseInt(expressionParts[0]);
        }
        builder.append(executionInstant);
        executionInstant += increment;
        while(executionInstant <= expressionPart.getUpperLimit()) {
            builder
                    .append(" ")
                    .append(executionInstant);
            executionInstant += increment;
        }

        return builder.toString();
    }

    @Override
    public void validate(final String expression, CronExpressionPart part) {

        final String[] expressionParts = expression.split(CronExpressionType.INCREMENT.getIdentifier());
        final int expectedNumberOfParts = 2;
        final String allValues = CronExpressionType.ALL_VALUES.getIdentifier();

        if(expressionParts.length != expectedNumberOfParts) {
            throw new InvalidCronElementExpression(part.getPartName(), expression, "Invalid expression format");
        }

        final String incrementUnit = expressionParts[1];
        if(isNumeric(incrementUnit) == false){
            throw new InvalidCronElementExpression(part.getPartName(), expression, "Only numbers are allowed in the increment");
        }

        final Integer increment = Integer.valueOf(incrementUnit);

        if(valueIsWithinAllowedInvervalForType(increment, part) == false){
            throw new InvalidCronElementExpression(part.getPartName(), expression, "Values out of range for type");
        }

        int executionInstant = part.getLowerLimit();
        if(expression.contains(allValues) == false) {
            if(isNumeric(expressionParts[0]) == false){
                throw new InvalidCronElementExpression(part.getPartName(), expression, "Only numbers or * are allowed in the initial moment");
            }

            executionInstant = Integer.parseInt(expressionParts[0]);
            if(executionInstant > part.getUpperLimit() || executionInstant < part.getLowerLimit()){
                throw new InvalidCronElementExpression(part.getPartName(), expression, "Values out of range for type");
            }
        }
    }

    @Override
    public Boolean supports(CronExpressionType expressionType) {
        return CronExpressionType.INCREMENT.equals(expressionType);
    }
}
