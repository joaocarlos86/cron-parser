package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.expressiontype.CronExpressionType;

import java.util.regex.Pattern;

public interface CronExpressionTypeToStringProcessor {
    public String handle(final String expression, final CronExpressionPart expressionPart);

    public void validate(String expression, CronExpressionPart part);

    public Boolean supports(CronExpressionType expressionType);

    default Boolean isNumeric(final String value){
        final Pattern onlyNumbersPattern = Pattern.compile("^[0-9]+$");
        return onlyNumbersPattern.matcher(value).find();
    }

    default Boolean valueIsWithinAllowedInvervalForType(final Integer value, CronExpressionPart part) {
        return value <= part.getUpperLimit() && value >= part.getLowerLimit();
    }
}
