package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.CronExpressionType;

import java.util.stream.IntStream;

public class RangeOfValuesExpressionTypeToStringProcessor implements CronExpressionTypeToStringProcessor {
    @Override
    public String handle(String expression, CronExpressionPart expressionPart) {
        final StringBuilder builder = new StringBuilder("");
        final String[] expressionParts = expression.split(CronExpressionType.RANGE_OF_VALUES.getIdentifier());
        final Integer initialExecution = Integer.parseInt(expressionParts[0]);
        final Integer lastExecution = Integer.parseInt(expressionParts[1]);

        builder.append(initialExecution);
        IntStream
                .rangeClosed(initialExecution + 1, lastExecution)
                .forEach(instant -> builder
                        .append(" ")
                        .append(instant)
                );

        return builder.toString();
    }

    @Override
    public void validate(String expression, CronExpressionPart part) {
        final String[] expressionParts = expression.split(CronExpressionType.RANGE_OF_VALUES.getIdentifier());
        final String start = expressionParts[0];
        final String end = expressionParts[1];

        if(isNumeric(start) == false) {
            throw new InvalidCronElementExpression(part.getPartName(), expression, "Only numbers are accepted for ranges of values");
        }

        if(isNumeric(end) == false) {
            throw new InvalidCronElementExpression(part.getPartName(), expression, "Only numbers are accepted for ranges of values");
        }

        final Integer initialExecution = Integer.parseInt(start);
        final Integer lastExecution = Integer.parseInt(end);

        IntStream
                .rangeClosed(initialExecution, lastExecution)
                .forEach(instant -> {
                            if(valueIsWithinAllowedInvervalForType(instant, part) == false){
                                throw new InvalidCronElementExpression(part.getPartName(), expression, "Values out of range for type");
                            }
                        }
                );
    }

    @Override
    public Boolean supports(CronExpressionType expressionType) {
        return CronExpressionType.RANGE_OF_VALUES.equals(expressionType);
    }
}
