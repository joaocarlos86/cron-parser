package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.CronExpressionType;

import java.util.stream.IntStream;

public class AllValuesExpressionTypeToStringProcessor implements CronExpressionTypeToStringProcessor {
    @Override
    public String handle(final String expression, final CronExpressionPart expressionPart) {
        final StringBuilder builder = new StringBuilder("");
        builder.append(expressionPart.getLowerLimit());

        IntStream
                .rangeClosed(expressionPart.getLowerLimit() + 1, expressionPart.getUpperLimit())
                .forEach(moment -> builder
                        .append(" ")
                        .append(moment));
        return builder.toString();
    }

    @Override
    public void validate(String expression, CronExpressionPart part) {
        if(CronExpressionType.ALL_VALUES.getIdentifier().equals(expression) == false) {
            throw new InvalidCronElementExpression(part.toString(), expression, "Expression should be \"*\"");
        }
    }

    @Override
    public Boolean supports(CronExpressionType expressionType) {
        return CronExpressionType.ALL_VALUES.equals(expressionType);
    }
}
