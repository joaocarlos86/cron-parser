package com.deliveroo.cron.expressiontype.processor;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.CronExpressionType;

import java.util.stream.IntStream;

public class MultipleValuesExpressionTypeToStringProcessor implements CronExpressionTypeToStringProcessor {
    @Override
    public String handle(String expression, CronExpressionPart expressionPart) {
        final StringBuilder builder = new StringBuilder("");
        final String[] expressionParts = expression.split(CronExpressionType.MULTIPLE_VALUES.getIdentifier());
        builder.append(expressionParts[0]);
        IntStream
                .range(1, expressionParts.length)
                .forEach(i -> builder
                        .append(" ")
                        .append(expressionParts[i])
                );

        return builder.toString();
    }

    @Override
    public void validate(final String expression, CronExpressionPart part) {
        final String[] expressionParts = expression.split(CronExpressionType.MULTIPLE_VALUES.getIdentifier());
        IntStream
                .range(0, expressionParts.length)
                .forEach(i -> {
                            String currentExpression = expressionParts[i];
                            if(isNumeric(currentExpression) == false){
                                throw new InvalidCronElementExpression(part.getPartName(), expression, "Only numbers are accepted for multiple values");
                            }

                            final Integer executionMoment = Integer.parseInt(currentExpression);
                            if(valueIsWithinAllowedInvervalForType(executionMoment, part) == false){
                                throw new InvalidCronElementExpression(part.getPartName(), expression, "Values out of range for type");
                            }
                        }
                );
    }

    @Override
    public Boolean supports(CronExpressionType expressionType) {
        return CronExpressionType.MULTIPLE_VALUES.equals(expressionType);
    }
}
