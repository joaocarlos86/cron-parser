package com.deliveroo.cron.expressiontype;

import com.deliveroo.cron.CronExpressionPart;
import com.deliveroo.cron.exception.InvalidCronElementExpression;
import com.deliveroo.cron.expressiontype.processor.*;

import java.util.LinkedList;
import java.util.List;

public class CronExpressionToString {

    private final List<CronExpressionTypeToStringProcessor> processors;

    public CronExpressionToString(){
        processors = new LinkedList<>();
        processors.add(new IncrementExpressionTypeToStringProcessor());
        processors.add(new MultipleValuesExpressionTypeToStringProcessor());
        processors.add(new RangeOfValuesExpressionTypeToStringProcessor());
        processors.add(new AllValuesExpressionTypeToStringProcessor());
        processors.add(new DefaultExpressionTypeToStringProcessor());
    }

    private CronExpressionTypeToStringProcessor getProcessor(CronExpressionType type) {
        return processors
                .stream()
                .filter(processor -> processor.supports(type))
                .findFirst()
                .orElseThrow(() -> new InvalidCronElementExpression("Invalid expression. Couldn't find any processor to handle the expression."));
    }

    public String getStringValue(final String expression, final CronExpressionType type, final CronExpressionPart part) {
        return getProcessor(type)
                .handle(expression, part);
    }

    public void validate(final String expression, final CronExpressionType type, final CronExpressionPart part) {
        getProcessor(type)
                .validate(expression, part);
    }
}
