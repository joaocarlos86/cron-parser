package com.deliveroo.cron.expressiontype;

import com.deliveroo.cron.exception.InvalidCronElementExpression;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public enum CronExpressionType {

    INCREMENT("/"),
    MULTIPLE_VALUES(","),
    RANGE_OF_VALUES("-"),
    ALL_VALUES("*"),
    SINGLE_VALUE("^[0-9]+$");

    private String identifier;

    CronExpressionType(final String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    private static List<CronExpressionType> getOrderedTypes(){
        final List<CronExpressionType> types = new LinkedList<>();
        types.add(INCREMENT);
        types.add(MULTIPLE_VALUES);
        types.add(RANGE_OF_VALUES);
        types.add(ALL_VALUES);
        types.add(SINGLE_VALUE);

        return types;
    }

    public static CronExpressionType getType(final String expression){
        final Optional<CronExpressionType> foundType = getOrderedTypes()
                .stream()
                .filter(type -> expression.contains(type.getIdentifier()))
                .findFirst();

        if(foundType.isPresent()){
            return foundType.get();
        } else {
            final Pattern pattern = Pattern.compile(SINGLE_VALUE.getIdentifier());
            if(pattern.matcher(expression).find()){
                return SINGLE_VALUE;
            }
        }

        throw new InvalidCronElementExpression("Invalid cron expression");
    }
}
