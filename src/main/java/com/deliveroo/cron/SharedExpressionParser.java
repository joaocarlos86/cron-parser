package com.deliveroo.cron;

import com.deliveroo.cron.expressiontype.CronExpressionToString;
import com.deliveroo.cron.expressiontype.CronExpressionType;

public class SharedExpressionParser {

    public String convertExpressionToString(final String expression, CronExpressionPart part) {
        final CronExpressionType type = CronExpressionType.getType(expression);

        CronExpressionToString expressionToString = new CronExpressionToString();
        return expressionToString.getStringValue(expression, type, part);
    }
}
