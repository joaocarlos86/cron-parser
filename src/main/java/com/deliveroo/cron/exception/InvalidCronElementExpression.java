package com.deliveroo.cron.exception;

public class InvalidCronElementExpression extends IllegalArgumentException {

    public InvalidCronElementExpression(final String message) {
        super(message);
    }

    public InvalidCronElementExpression(final String partType, final String expression, final String reason) {
        super("Impossible to build cron element | type = %s | value %s | reason = %s"
                .format(partType, expression, reason));
    }
}
