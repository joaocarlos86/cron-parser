package com.deliveroo.cron;

public class Main {
    public static void main(String[] args) {

        if(args.length != 1){
            throw new IllegalArgumentException("Please inform the cron expression in the format \"*/25 0 1,15 * */5 /usr/bin/find\"");
        }

        CronExpressionParser parser = new CronExpressionParser(args[0]);
        System.out.println(parser.toString());
    }
}
