package com.deliveroo.cron;

import com.deliveroo.cron.expressiontype.CronExpressionToString;
import com.deliveroo.cron.expressiontype.CronExpressionType;

public class SharedExpressionValidator {

    public Boolean validateExpression(final String expression, CronExpressionPart part) {
        final CronExpressionType type = CronExpressionType.getType(expression);
        final CronExpressionToString expressionToString = new CronExpressionToString();
        expressionToString.validate(expression, type, part);

        return true;
    }
}
