package com.deliveroo.cron;

import com.deliveroo.cron.exception.InvalidCronElementExpression;

public class CronExpressionParser {

    private final SharedExpressionParser sharedExpressionParser;
    private final String[] expressionParts;

    public CronExpressionParser(String expression) {
        expressionParts = expression.split("\\s");
        sharedExpressionParser = new SharedExpressionParser();

        validateExpressionParts(expressionParts);
    }

    private void validateExpressionParts(String[] expressionParts) {
        if(expressionParts.length != 6) {
            throw new InvalidCronElementExpression("The cron expression should have six parts.");
        }

        final SharedExpressionValidator validator = new SharedExpressionValidator();
        validator.validateExpression(expressionParts[0], CronExpressionPart.MINUTE);
        validator.validateExpression(expressionParts[1], CronExpressionPart.HOUR);
        validator.validateExpression(expressionParts[2], CronExpressionPart.DAY_OF_MONTH);
        validator.validateExpression(expressionParts[3], CronExpressionPart.MONTH);
        validator.validateExpression(expressionParts[4], CronExpressionPart.DAY_OF_WEEK);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("");

        final String minuteExpression = sharedExpressionParser.convertExpressionToString(expressionParts[0], CronExpressionPart.MINUTE);
        final String hourExpression = sharedExpressionParser.convertExpressionToString(expressionParts[1], CronExpressionPart.HOUR);
        final String dayOfMonthExpression = sharedExpressionParser.convertExpressionToString(expressionParts[2], CronExpressionPart.DAY_OF_MONTH);
        final String monthExpression = sharedExpressionParser.convertExpressionToString(expressionParts[3], CronExpressionPart.MONTH);
        final String dayOfWeekExpression = sharedExpressionParser.convertExpressionToString(expressionParts[4], CronExpressionPart.DAY_OF_WEEK);

        builder.append("minute\t\t").append(minuteExpression);
        builder.append("\n");
        builder.append("hour\t\t").append(hourExpression);
        builder.append("\n");
        builder.append("day of month\t").append(dayOfMonthExpression);
        builder.append("\n");
        builder.append("month\t\t").append(monthExpression);
        builder.append("\n");
        builder.append("day of week\t").append(dayOfWeekExpression);
        builder.append("\n");
        builder.append("command\t\t").append(expressionParts[5]);
        return builder.toString();
    }
}
