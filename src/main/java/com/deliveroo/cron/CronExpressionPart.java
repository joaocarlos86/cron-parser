package com.deliveroo.cron;

public enum CronExpressionPart {
    MINUTE("minute",0,59),
    HOUR("hour",0, 23),
    DAY_OF_MONTH("day of month",1,31),
    MONTH("month",1,12),
    DAY_OF_WEEK("day of week",1,7);

    private final String partName;
    private final Integer lowerLimit;
    private final Integer upperLimit;

    CronExpressionPart(final String partName, final Integer lowerLimit, final Integer upperLimit) {
        this.partName = partName;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
    }

    public String getPartName() {
        return this.partName;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public Integer getUpperLimit() {
        return upperLimit;
    }
}
